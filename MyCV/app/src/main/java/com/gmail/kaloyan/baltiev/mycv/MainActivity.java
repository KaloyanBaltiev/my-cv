package com.gmail.kaloyan.baltiev.mycv;

/**
 * MainActivity.java
 *
 * Kaloyan A. Baltiev
 * kaloyan.baltiev@gmail.com
 *
 * Set the CV image, initialize buttons and create personal records
 */

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private PersonalInformation personalInformation;
    private List<WorkExperience> workExperience = new ArrayList<>();
    private List<Education> educations = new ArrayList<>();
    private List<AdditionalCourse> additionalCourses = new ArrayList<>();
    private PersonalSkills personalSkills;
    private AdditionalDetails additionalDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        createRecords();
        setImage();
        initializeButtons();

    }

    // set the buttons to listen for clicks, on click pass the string information to the second activity
    private void initializeButtons() {
        final Button personalDetailsBtn = findViewById(R.id.personalDetailsBtn); // ref. to the button
        personalDetailsBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), // send to second activity
                        SecondActivity.class);
                startIntent.putExtra("info", personalInformation.toString()); // pass the info string
                startActivity(startIntent);
            }
        });

        final Button workExperienceBtn = findViewById(R.id.workExperienceBtn);
        workExperienceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),
                        SecondActivity.class);

                String info = "";
                for (WorkExperience exp : workExperience) {
                    info += exp.toString();
                    info += "\n";
                }

                startIntent.putExtra("info", info);
                startActivity(startIntent);
            }
        });

        final Button educationBtn = findViewById(R.id.educationBnt);
        educationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),
                        SecondActivity.class);

                String info = "";
                for (Education exp : educations) {
                    info += exp.toString();
                    info += "\n";
                }

                startIntent.putExtra("info", info);
                startActivity(startIntent);
            }
        });

        final Button additionalCoursesBtn = findViewById(R.id.additionalCoursesBtn);
        additionalCoursesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),
                        SecondActivity.class);

                String info = "";
                for (AdditionalCourse exp : additionalCourses) {
                    info += exp.toString();
                    info += "\n";
                }

                startIntent.putExtra("info", info);
                startActivity(startIntent);
            }
        });

        final Button personalSkillsBtn = findViewById(R.id.personalSkillsBtn);
        personalSkillsBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),
                        SecondActivity.class);
                startIntent.putExtra("info", personalSkills.toString());
                startActivity(startIntent);
            }
        });

        final Button additionalDetailsBtn = findViewById(R.id.additionalDetailsBtn);
        additionalDetailsBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(),
                        SecondActivity.class);
                startIntent.putExtra("info", additionalDetails.toString());
                startActivity(startIntent);
            }
        });
    }

    // find the image in drawable folder and set to screen
    private void setImage() {
        ImageView cvImage = findViewById(R.id.cvImageView);
        int imageResource = getResources().getIdentifier("@drawable/kala", null, this.getPackageName());
        cvImage.setImageResource(imageResource);
    }

    // create all personal information records
    private void createRecords() {

        personalInformation = new PersonalInformation(
                "Kaloyan Baltiev",
                "0899342471",
                "kaloyan.baltiev@gmail.com",
                "Bulgaria",
                "Sofia"
        );

        WorkExperience workExperience1 = new WorkExperience(
                "September 2015",
                "September 2018",
                "Associate",
                "Home and office construction, finishing work",
                "BRUCE VAN ALLEN, INC.",
                "Massachusetts, USA",
                "Architecture and construction"
                );

        WorkExperience workExperience2 = new WorkExperience(
                "September 2008",
                "May 2013",
                "Technician",
                "Design, installation and maintenance of automated home security systems and remote\n" +
                        "video surveillance",
                "Falcon Pro",
                "Veliko Tarnovo, Bulgaria",
                "Security"
        );

        // add to the list
        workExperience.add(workExperience1);
        workExperience.add(workExperience2);

        Education education1 = new Education(
                "September 2008",
                "May 2013",
                "Telecommunications Engineering",
                "Technical University of Gabrovo",
                "Gabrovo",
                "Bachelor's degree"

        );

        Education education2 = new Education(
                "September 2004",
                "May 2008",
                "Telecommunications",
                "Professional High School of Electronics \"A.S. Popov\"",
                "Veliko Tarnivo",
                "Professional"

        );

        // add to the list
        educations.add(education1);
        educations.add(education2);

        AdditionalCourse additionalCourse1 = new AdditionalCourse(
                "Swift Academy\n" +
                        "JAVA Course\n" +
                        "Java for beginners\n" +
                        "http://swift.bg/certificates/kaloyanbaltiev/923d0c1d.pdf"
        );

        AdditionalCourse additionalCourse2 = new AdditionalCourse(
                "Harvard Extension\n" +
                        "School\n" +
                        "CS50's\n" +
                        "Introduction to Computer Science\n" +
                        "https://courses.edx.org/certificates/4a2d3e93024045b1a4eb2a86a2bc77bb"
        );

        // add to the list
        additionalCourses.add(additionalCourse1);
        additionalCourses.add(additionalCourse2);

        personalSkills = new PersonalSkills(
                "Bulgarian",
                "English",
                "Main programming language:\n" +
                        "Java.\n" +
                        "\n"+
                        "Other programming languages experience:\n" +
                        "C, Python, JavaScript.\n" +
                        "\n"+
                        "Experience with Git, Gitlab, Github, SQL,\n" +
                        "HTML + CSS, Spring +\n" +
                        "Thyme Leaf.\n" +
                        "\n"+
                        "IDE's:\n" +
                        "Intellij, Android Studio,\n" +
                        "Eclipse, Cloud9 IDE",
                "Knowledge in electronics. Design, build and test electronic circuits and modules. Lots of\n" +
                        "school and personal projects."
        );

        additionalDetails = new AdditionalDetails(
                "Final project for Swift Academy — Build application with Spring MVC.\n" +
                        "Java Web App, creates and uploads personal information records on remote server.\n" +
                        "The app can search for a person by name, open personal record and calculate social aid\n" +
                        "amount if eligible.\n" +
                        "https://gitlab.com/KaloyanBaltiev/final-project-swift\n" +
                        "\n"+
                        "GoRide — Android GPS tracker app\n" +
                        "The application, checks the location of the client\n" +
                        "records coordinates and creates a route. The route is plotted on Google Maps, modified or\n" +
                        "deleted.\n" +
                        "https://gitlab.com/KaloyanBaltiev/goride\n" +
                        "https://youtu.be/raCzr2f7tM8"
        );
    }

}
