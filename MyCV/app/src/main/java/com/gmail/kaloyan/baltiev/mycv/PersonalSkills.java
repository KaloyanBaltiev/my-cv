package com.gmail.kaloyan.baltiev.mycv;

public class PersonalSkills {

    private String motherTongue;
    private String languages;
    private String computerSkills;
    private String otherSkills;

    public PersonalSkills(String motherTongue, String languages, String computerSkills, String otherSkills) {
        this.motherTongue = motherTongue;
        this.languages = languages;
        this.computerSkills = computerSkills;
        this.otherSkills = otherSkills;
    }

    @Override
    public String toString() {

        String info = String.format("Mother tongue: %s\n", motherTongue);
        info += String.format("Languages: %s\n", languages);
        info += String.format("Computer skills: %s\n", computerSkills);
        info += String.format("Other skills: %s\n", otherSkills);

        return info;
    }
}
