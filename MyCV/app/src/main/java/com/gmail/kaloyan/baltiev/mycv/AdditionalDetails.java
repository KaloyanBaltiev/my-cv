package com.gmail.kaloyan.baltiev.mycv;

public class AdditionalDetails {

    private String additionalDetails;

    public AdditionalDetails(String additionalDetails) {
        this.additionalDetails = additionalDetails;
    }

    @Override
    public String toString() {

        String info = String.format("Additional Details: %s\n", additionalDetails);
        return info;
    }
}
