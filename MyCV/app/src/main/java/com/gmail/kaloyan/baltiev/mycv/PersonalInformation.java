package com.gmail.kaloyan.baltiev.mycv;

public class PersonalInformation {
    private String name;
    private String phone;
    private String email;
    private String citizenship;
    private String city;

    public PersonalInformation(String name, String phone, String email, String citizenship, String city) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.citizenship = citizenship;
        this.city = city;
    }

    @Override
    public String toString() {

        String info = String.format("Name: %s\n", name);
        info += String.format("Phone: %s\n", phone);
        info += String.format("Email: %s\n", email);
        info += String.format("Citizenship: %s\n", citizenship);
        info += String.format("City: %s\n", city);

        return info;
    }
}
