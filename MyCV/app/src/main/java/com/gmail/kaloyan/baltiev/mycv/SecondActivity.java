package com.gmail.kaloyan.baltiev.mycv;

/**
 * SecondActivity.java
 * <p>
 * Kaloyan A. Baltiev
 * kaloyan.baltiev@gmail.com
 * <p>
 * Initialize buttons and display incoming data
 */

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        initializeButtons();
        displayIncomingInfo();

    }

    // display incoming info in a text view
    private void displayIncomingInfo() {
        TextView incomingDataView = findViewById(R.id.textView);
        Intent incomingIntent = getIntent();
        String incomingDataStr = incomingIntent.getStringExtra("info");
        incomingDataView.setText(incomingDataStr);
    }

    // set the buttons to listen for clicks
    private void initializeButtons() {
        Button mainActivityBtn = findViewById(R.id.goBackBtn);
        mainActivityBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent startIntent = new Intent(getApplicationContext(), // send back to main
                        MainActivity.class);
                startActivity(startIntent);
            }
        });
    }
}
