package com.gmail.kaloyan.baltiev.mycv;

public class Education {
    private String dateStart;
    private String dateEnd;
    private String titleOfQualificationAwarded;
    private String nameAndTypeOfOrganisation;
    private String location;
    private String level;

    public Education(String dateStart, String dateEnd, String titleOfQualificationAwarded,
                     String nameAndTypeOfOrganisation, String location, String level) {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.titleOfQualificationAwarded = titleOfQualificationAwarded;
        this.nameAndTypeOfOrganisation = nameAndTypeOfOrganisation;
        this.location = location;
        this.level = level;
    }

    @Override
    public String toString() {

        String info = String.format("Dates: %s - %s\n", dateStart, dateEnd);
        info += String.format("Title of qualification awarded: %s\n", titleOfQualificationAwarded);
        info += String.format("Name and type of organisation: %s\n", nameAndTypeOfOrganisation);
        info += String.format("Location: %s\n", location);
        info += String.format("Level %s\n", level);

        return info;

    }
}
