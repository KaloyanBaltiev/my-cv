package com.gmail.kaloyan.baltiev.mycv;

public class AdditionalCourse {

    private String course;

    public AdditionalCourse(String course) {
        this.course = course;
    }

    public String toString() {

        String info = String.format("Course: %s\n", course);
        return info;
    }
}
