package com.gmail.kaloyan.baltiev.mycv;

public class WorkExperience {

    private String dateStart;
    private String dateEnd;
    private String occupationPositionHeld;
    private String mainActivitiesAndResponsibilities;
    private String nameOfEmployer;
    private String location;
    private String typeOfBusinessOrSector;

    public WorkExperience(String dateStart, String dateEnd, String occupationPositionHeld,
                          String mainActivitiesAndResponsibilities, String nameOfEmployer,
                          String location, String typeOfBusinessOrSector) {
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.occupationPositionHeld = occupationPositionHeld;
        this.mainActivitiesAndResponsibilities = mainActivitiesAndResponsibilities;
        this.nameOfEmployer = nameOfEmployer;
        this.location = location;
        this.typeOfBusinessOrSector = typeOfBusinessOrSector;
    }

    @Override
    public String toString() {

        String info = String.format("Dates: %s - %s\n", dateStart, dateEnd);
        info += String.format("Occupation or position held: %s\n", occupationPositionHeld);
        info += String.format("Main activities and responsibilities: %s\n", mainActivitiesAndResponsibilities);
        info += String.format("Name of employer: %s\n", nameOfEmployer);
        info += String.format("Location: %s\n", location);
        info += String.format("Type of business or sector: %s\n", typeOfBusinessOrSector);

        return info;

    }
}
