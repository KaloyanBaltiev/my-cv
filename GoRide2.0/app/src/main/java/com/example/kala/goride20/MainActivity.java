package com.example.kala.goride20;
/**
 * MainActivity.java
 *
 * Kaloyan A. Baltiev
 * kaloyan.baltiev@gmail.com
 *
 * Handles the GPS_Service.java broadcast; starts new entries to the database
 */

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btn_ride, btn_stop, btn_see;
    private TextView textView;
    private BroadcastReceiver broadcastReceiver;
    private EditText editText_ride;
    public MyDBHandler dbHandler;
    String rideName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // reference to the objects
        dbHandler = new MyDBHandler(this, null, null, 7);
        btn_ride = (Button) findViewById(R.id.button_ride);
        btn_stop = (Button) findViewById(R.id.button_stop);
        btn_see = (Button) findViewById(R.id.button_see);
        textView = (TextView) findViewById(R.id.textView);
        editText_ride = (EditText) findViewById(R.id.editText_ride);

        // handles the permissions check
        if(!runtime_permissions()) {
            enable_buttons();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // turn on the broadcast receiver
        if(broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    // display the coordinate broadcast
                    textView.append("\n" +intent.getExtras().get("coordinates"));

                    // save the coordinate among with information
                    double latitude = intent.getDoubleExtra("latitude", 0.00000);
                    double longitude = intent.getDoubleExtra("longitude", 0.00000);
                    GeoPoint geoPoint = new GeoPoint(latitude, longitude, rideName);
                    dbHandler.addProduct(geoPoint);
                }
            };
        }

        // filter receiver
        registerReceiver(broadcastReceiver,new IntentFilter("location_update"));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // turn off receiver
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
    }


    // enables the buttons
    private void enable_buttons() {

        // button ride
        btn_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // ger the ride's name from the user and start service
                rideName = editText_ride.getText().toString();
                if(TextUtils.isEmpty(rideName)) {
                    Toast.makeText(getApplicationContext(), "Please enter the name of the ride", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent i = new Intent(getApplicationContext(),GPS_Service.class);
                startService(i);
            }
        });

        // button stop
        btn_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // reset the text view and stop service
                editText_ride.setText("");
                textView.setText("Coordinates: ");
                Intent i = new Intent(getApplicationContext(),GPS_Service.class);
                stopService(i);

            }
        });

        // button see
        btn_see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i =new Intent(getApplicationContext(),LibraryOfMapsActivity.class);
                startActivity(i);

            }
        });
    }

    /**
     *
     * what this peace of code does is basically checking if the device SDK >= 23 and
     * requests location permission or < 23 and requests location permission :)
     */
    private boolean runtime_permissions() {
        if(Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED){

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION},100);

            return true;
        }
        return false;
    }
}
