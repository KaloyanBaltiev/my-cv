package com.example.kala.goride20;
/**
 * MyDBHandler.java
 *
 * Kaloyan A. Baltiev
 * kaloyan.baltiev@gmail.com
 *
 * Handles all the communication between the SQL database and the rest of the app
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

// extends SQLiteOpenHelper
public class MyDBHandler extends SQLiteOpenHelper{
    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "geopointDB.db";
    public static final String TABLE_GEOPOINTS = "geopoints";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGITUDE = "longitude";
    public static final String COLUMN_NAME = "name";



    // need to pass database information along to superclass
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    // create a new table
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create query
        String query = "CREATE TABLE " + TABLE_GEOPOINTS + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_LATITUDE + " REAL, " +
                COLUMN_LONGITUDE + " REAL, " +
                COLUMN_NAME + " TEXT " +
                ");";
        db.execSQL(query);
    }

    // on update
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GEOPOINTS);
        onCreate(db);
    }

    // add a new row to the database
    public void addProduct(GeoPoint geoPoint){
        ContentValues values = new ContentValues();
        values.put(COLUMN_LATITUDE, geoPoint.get_lat());
        values.put(COLUMN_LONGITUDE, geoPoint.get_lng());
        values.put(COLUMN_NAME, geoPoint.get_name());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_GEOPOINTS, null, values);
        db.close();
    }

    // delete a ride from the database
    public void deleteGeoPoints(String name){
        SQLiteDatabase db = getWritableDatabase();
        String query = "DELETE FROM " + TABLE_GEOPOINTS + " WHERE " + COLUMN_NAME + "=\"" + name + "\";";
        db.execSQL(query);
        db.close();
    }

    // get rides information
    public ArrayList<String> databaseMaps(){
        ArrayList<String> maps = new ArrayList<>();
        SQLiteDatabase db = getWritableDatabase();

        // delete query
        String query = "SELECT * FROM " + TABLE_GEOPOINTS + " WHERE 1";

        // cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);

        // move to the first row in your results
        c.moveToFirst();

        // position after the last row means the end of the results
        while (!c.isAfterLast()) {

            // null could happen if empty constructor was used
            if (c.getString(c.getColumnIndex("name")) != null) {
                String point = c.getString(c.getColumnIndex("name"));
                if (!maps.contains(point)) {
                    maps.add(point);
                }
            }
            c.moveToNext();
        }
        db.close();
        return maps;
    }



    // getting all points from the the particular entry
    public ArrayList<LatLng> getAllLatLng(String name) {
        ArrayList<LatLng> contactList = new ArrayList<>();

        // select all query
        String query = "SELECT * FROM " + TABLE_GEOPOINTS + " WHERE " + COLUMN_NAME + "=\"" + name + "\";";

        SQLiteDatabase db = getWritableDatabase();
        Cursor c = db.rawQuery(query, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                LatLng data = new LatLng(c.getDouble(1), c.getDouble(2));

                //   Adding contact to list
                contactList.add(data);
            } while (c.moveToNext());
        }
        db.close();

        // return database object list
        return contactList;
    }
}
